---
layout: handbook-page-toc
title: "Technical Marketing"
description: "Learn more about the purpose, process and output of GitLab's Technical Marketing."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
#### Purpose (Why)
Technical Marketing exists to create content meant to entice prospects into the top of the funnel by expressing the technical capabilities of the product as values to our target audiences.

#### Process (How)
We do this by teaching our audiences about modern software delivery methods and how it can be valuable to them, introducing them to new concepts which can help them achieve their goals, and by showcasing the capabilities of the product for the use cases which our audiences care about.

#### Output (What)
We produce demos, videos, workshops, tutorials, technical white papers, blogs, conference presentations, and webinars.

## Asset to stakeholder matrix

We create several types of technical assets which can be used by many potential stakeholders. See details on the [marix of assets:stakeholders](assets2stakeholders.html)

## Demos

One form of output is demo videos to help show the value GitLab can bring to customers.
* Go to the [Learn@GitLab page](/learn/) to see what's available.
* Learn about our [demo creation and publishing practices](./howto/create-and-publish-demos.html)

#### Updating demos

1. **Can we update a current video with new features?** \
Yes we can, however. . . 

1. **What is the process for updating videos when new features come out?** \
. . . because of the end to end data trail that flows throughout the product, it can be difficult sometimes to just add new parts into existing demos, depending on what's being shown, and so sometimes requires extensive, if not complete, redo. Because of this and the high frequency of changes we don't make updates for EVERY new feature/change. We look for when there are significant enough features/changes that affect the value we are demonstrating. We watch ourselves, but also look to PMM and PM and sales to indicate that updates should be done. \
\
Anyone can [open an SM request](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=A-SM-Support-Request) for us to update a demo. If there are significant enough changes then we will put the update into the backlog and work with stakeholders to set the right priority to get it completed.

1. **Can we update/create a new video to address a different persona/theme?** \
Switching a demo to a different persona usually should mean a new demo/video, as changing the target persona should result in changes in the pain points and story, and hence likely the video/screenshots that are shown. It is certainly possible that the existing footage can fit a new persona's story, or that we can change the story to include multiple personas. But more likely than not, if this is easy to do to an existing demo then it is probably because it wasn't properly focused on a persona in the first place. 

## Prioritizing work
Our work is mainly driven and prioritized by the following Marketing defined [Customer Use Cases](/handbook/use-cases/) and can be tracked on the [Use Case Driven GTM page](/handbook/marketing/strategic-marketing/usecase-gtm/). Specifically, our deliverables contributing to this effort are specified on the [Technical Marketing BOM Elements page](/handbook/marketing/strategic-marketing/usecase-gtm/bom/tmm.html).

When Analyst Reports come along then we prioritize providing and reviewing technical responses and most importantly we are the DRI for any demos that are required.

## What are we currently working on?
View the [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm) to see what the TMM team is working on.

## Making a Request
To make a request of the Technical Marketing team please [open an issue and fill in the Strategic Marketing Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request). We will respond to it using [our request management flow](/handbook/marketing/strategic-marketing/#requesting-strategic-marketing-team-helpsupport).

## Chat, say hi, quick question
Slack channel: #technical-marketing

## Which Technical Marketing Manager?

Each TMM is listed with their areas of primary responsibility, but all TMM's should be able to help in other areas of the product as well:

  - [Tye Davis](/company/team/#TyeD19) - Manager TMM, Agile
  - [Itzik Gan-Baruch](/company/team/#itzikgb), CI, CD (acting)
  - [Fernando Diaz](/company/team/#fjdiaz), DevSecOps
  - [William Arias](/company/team/#warias) - SCM, DevOps Platform
  - [Cesar Saavedra](/company/team/#csaavedra1) - GitOps/IaC
  - [Mahesh](/company/team/#Mahesh), Sr. Director, SM

## Technical Marketing Howto's
* Adding comparison pages ([instructions](/handbook/marketing/inbound-marketing/digital-experience/website/#creating-a-devops-tools-comparison-page), [video](https://youtu.be/LH4lKT-H2UU))
* [Using and creating simulation demos](/handbook/marketing/strategic-marketing/demo/sim-demos/)
* Creating a Google (GCP) GKE cluster for GitLab demo
* [Creating an AWS EKS cluster for a GitLab demo](./howto/eks-cluster-for-demo.html)
* [Conference booth demo setup](/handbook/marketing/strategic-marketing/demo/conference-booth-setup/)
* [Adding CTAs to Learn@GitLab YouTube videos](./howto/add-ctas-to-learn-videos.html)
* [Creating and publishing demo videos](./howto/create-and-publish-demos.html)
* [Autogenerating Comparison Infographics](./howto/autogen-comparison-infographics.html)
* [Gitlab.com logins for demos](/handbook/marketing/strategic-marketing/technical-marketing/gl-com-demo-logins/)
* [Contributing to Learn@GitLab](./howto/contributing-to-learn-at-gl.html)

## Making better videos

With the internet's attention span, the more successful videos are short form. GitLab Marketing's has adjusted it's video strategy to focus on short form, educational videos. With that move we've begun to learn about how to make better videos. The ["Making better videos" page](./making_better_videos.html) covers our research and pointers we are going to do our best to follow.
